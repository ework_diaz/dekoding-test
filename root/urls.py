from django.urls import path

from .views import (
    IndexView,
    FrontendView,
    BackendView,
)


urlpatterns = [
    path('', IndexView.as_view(), name="index"),
    path('frontend/', FrontendView.as_view(), name="frontend"),
    path('backend/', BackendView.as_view(), name="backend"),
]
